package com.cloudsenz.springbootsample.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

    @GetMapping(path = "/")
    public String helloWorld(){
        return "index";
    }
}
